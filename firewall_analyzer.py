from re import split, match
import datetime
import os
import json
import socket
from netaddr import IPNetwork, IPAddress

logdata = {}
parameters = {}
nstable = {}


def get_domain_name(ip):
    """
    get revers name of ip
    :param ip:  str
    :return: string revers domain name or ip
    """
    #print("ip",ip)
    if ip in nstable:
        return nstable[ip]
    elif IPAddress(ip) in IPNetwork("100.64.0.0/10"):
        return ""
    else:
        try:
            #print("name", ip)
            name = socket.gethostbyaddr(ip)[0]
            nstable[ip] = name
            return name
        except:
            #socket.gethostbyaddr(ip)[0]
            nstable[ip] = ip
            return ip


def getdata_from_file(fname, alldata):
    """

    :type fname: log file name
    :type alldata: dict with/out data from logs after analysis
    :return dict log data after analysis
    like:
    {"ACTION:DIRECTION:PROTOCOL:PORT", [#Requests for {ACTION:DIRECTION:PROTOCOL:PORT},
                            'Last date in log file for {ACTION:DIRECTION:PROTOCOL:PORT}',
                            'List of IP address for {ACTION:DIRECTION:PROTOCOL:PORT}',
                            'netsh string ready to open PROTOCOL:PORT in DIRECTION for List of IP'],...}

    {ALLOW:RECIVE:TCP:443, [9, '2020-02-25 08:23:59', '192.168.2.155 ' ,
    'netsh advfirewall firewall Add rule name="Allow port 443" dir=out protocol=tcp remoteport=443 action=allow remoteip=192.168.2.155'],
     DROP:SEND:UDP:88 [8, '2020-02-24 23:03:49', '192.168.2.15 192.168.2.4 ', 'netsh ...'],...}

    used fields from windows firewall log file and (column's orders).
    +action (2) protocol (3) ; src-ip (4) ; dst-ip (5); src-port (6); dst-port (7); path (16)
    """
    i = 0
    for line in split("\n", open(fname, "r").read()):
        char = "\x00"
        cells = split(" ", line.replace(char, ''))
        if cells[0] == "" or len(cells) < 16 or cells[0][0] == "#": continue
        lname = cells[2] + ":" + cells[16] + ":" + cells[3]
        lname += ":" + cells[7]
        if cells[4] == "-" or cells[5] == "-":
            continue
        # print(lname)
        if alldata.get(lname) == None:
            alldata[lname] = [0, "", "", ""]
        alldata[lname][0] += 1
#       print(cells[4], cells[5],alldata[lname])
        if cells[16] == "SEND":
#           print("s", cells[5])
            name = get_domain_name(cells[5])
            if name not in str(alldata[lname][2]):
                alldata[lname][2] += name

        if cells[16] == "RECEIVE":
#           print("r",cells[4])
            name = get_domain_name(cells[4])
            if name not in str(alldata[lname][2]):
                alldata[lname][2] += name

        if alldata[lname][2][-1] != " ":
            alldata[lname][2] += " "

        if alldata[lname][1] < cells[0]:
            alldata[lname][1] = cells[0] + " " + cells[1]

        alldata[lname][3] = (
                "& cmd /c 'netsh advfirewall firewall Add rule name=\"%s\" dir=%s protocol=%s remoteport=%s action=allow" %
                ("Allow port " + cells[7],
                 "out" if cells[16] == "SEND" else "in",
                 cells[3].lower(),
                 cells[7])
        )
        if not (i % 10):
            save_dict2file(nstable, "firewall_analyzer.resolve")
        i += 1
    return alldata


def save_dict2file(dict, file):
    try:
        file = parameters["dict_folder"] + "\\" + file
    except:
        pass
    with open(file, 'w') as fp:
        json.dump(dict, fp)
    fp.close()


def load_dict4file(file):
    try:
        file = parameters["dict_folder"] + "\\" + file
    except:
        pass
    with open(file, 'r') as fp:
        return json.load(fp)


# save_dict2file(gdata,"gdata.save")
parameters = load_dict4file("dicts\\firewall_analyzer.conf")
try:
    nstable = load_dict4file("firewall_analyzer.resolve")
except:
    nstable = {"-": "-", "127.0.0.1": "localhost", "::1": "localhostV6"}
    print("resolve file not found")

for server in split("\n", open("dicts\servers.lst", "r").read()):
    print("#starting with", server)
    if not parameters["logline"]:
        print("Invoke -Command -ComputerName %s -ScriptBlock\n{" % server)
    if parameters["renew"]:
        logdata = {}
    else:
        try:
            logdata = load_dict4file(server + ".dict")
        except:
            print("#" + server + ".dict not found. Renew data")
            logdata = {}
    if len(logdata) == 0:
        path = ("\\\\" if server != "" else "") + server + parameters["dir"]
        print("#" + path)
        try:
            for file in os.listdir(path):
                print("#" + path + file)
                logdata = getdata_from_file(path + file, logdata)
        except:
            print("error in", server)

        save_dict2file(logdata, server + ".dict")

    for d in sorted(logdata, key=logdata.get, reverse=True):
        if logdata[d][0] < parameters["min_count"]: break
        # print(gdata["filter"], str(d) + str(alldata[d]))
        if match(parameters["filter"], str(d) + " " + str(logdata[d][:3])) != None:
            if parameters["logline"]:
                print(d, logdata[d][:3])
            else:
                print(logdata[d][3] + " remoteip=" + logdata[d][2].replace(" ", ",").strip(",") + "'")
    if not parameters["logline"]:
        print("}")
    print("#finish with", server)
